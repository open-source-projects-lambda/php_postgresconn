<!DOCTYPE html>
<html>
<body>

<h1>My first PHP page</h1>

<?php

if (file_exists('vendor/autoload.php')) {
    require_once('vendor/autoload.php');
}

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

include("./db_connect.php");
echo "Hello World!";
?>

</body>
</html>